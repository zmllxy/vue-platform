const comOps = {
  rail: {
    background: '#4f525d',
    opacity: 0,
    /** Rail's size(Height/Width) , default -> 6px */
    size: '6px',
    /** Specify rail and bar's border-radius, or the border-radius of rail and bar will be equal to the rail's size. default -> false **/
    specifyBorderRadius: false,
    /** Rail the distance from the two ends of the X axis and Y axis. */
    gutterOfEnds: '2px',
    /** Rail the distance from the side of container. */
    gutterOfSide: '2px',
    /** Whether to keep rail show or not, default -> false, event content height is not enough */
    keepShow: true
  },
  bar: {
    /** 是否只在滚动的时候现实滚动条 */
    onlyShowBarOnScroll: true,
    /** 是否保持显示 */
    keepShow: true,
    /** 背景色 */
    background: '#4f525d',
    /**  透明度  */
    opacity: 0.6,
    /** 当你鼠标移动到滚动条的时候滚动条的样式， 返回一个style对象， 和现在的对象融合 */
    hoverStyle: false
  }
}

export default comOps
