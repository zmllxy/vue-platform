// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import i18n from './i18n/locales'
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from './utils/axiosutils'
import filters from './filters'
import store from './store'
import qs from 'qs'
import ElementUI from 'element-ui'
import vuescroll from 'vuescroll'
import 'element-ui/lib/theme-chalk/index.css'
import 'font-awesome/css/font-awesome.css'
import './assets/icons/iconfont/sysmIcon/iconfont.css'
import './assets/icons/iconfont/sysmIcon/iconfontbases.css'
import 'vuescroll/dist/vuescroll.css'
import './components/com/install'

// 水波纹指令
import vueWaves from './directives/waves'
import dscrollBar from './directives/dscrollbar'

// 引入mockjs
import '@/mock/index.js'
require('../static/gmap/gl.min.js')

Vue.prototype.$axios = axios

Vue.use(ElementUI)
Vue.use(vuescroll)
Vue.use(vueWaves)
Vue.use(dscrollBar)

Vue.config.productionTip = false

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

new Vue({
  i18n,
  router,
  axios,
  store,
  qs,
  render: h => h(App)
}).$mount('#app')
