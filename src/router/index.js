import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import Cookies from 'js-cookie'
import whiteList from './whiteList'
import routes from './baseRouter'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// 页面刷新时，重新赋值
if (Cookies.get('username')) {
  store.dispatch('user/relogin')
}

Vue.use(Router)

/**
 * 路由 VueRouter
 *
 * @type {VueRouter}
 */
const router = new Router({
  mode: 'history',
  routes: routes
})

/**
 * 路由跳转前验证
 */
router.beforeEach((to, from, next) => {
  // 判断用户是否登录
  if (Cookies.get('username')) {
    // 如果当前处于登录状态，并且跳转地址为login，则自动跳回系统首页
    // 这种情况出现在手动修改地址栏地址时
    if (to.path === '/login') {
      console.log('手动修改地址栏地址时' + to.path)
      router.replace('/home')
    } else {
      // 页面跳转前先判断是否存在权限列表，如果存在则直接跳转，如果没有则请求一次
      console.log('页面跳转前先判断是否存在权限列表:', store.state.permission.menuList)
      if (store.state.permission.menuList.length === 0) {
        // 获取权限列表，如果失败则跳回登录页重新登录
        store.dispatch('permission/getPermission').then(res => {
          console.log('获取权限列表:', res)
          // 匹配并生成需要添加的路由对象
          routerMatch(res, router).then(res => {
            console.log('处理后的路由:', res)
            router.addRoutes(res)
            console.log('路由对象:', router)
          })
          NProgress.start()
          next()
        }).catch(() => {
          console.log('登录错误')
          store.dispatch('sysm/logout').then(() => {
            router.replace('/login')
          })
        })
      } else {
        // 如果跳转页面存在于路由中则进入，否则跳转到404
        // 因为可以通过改变url值进行访问，所以必须有该判断
        console.log('如果跳转页面存在于路由中则-进入:', to.matched, to.path)
        if (to.matched.length) {
          NProgress.start()
          next()
        } else {
          router.replace('/error/builds')
        }
      }
    }
  } else {
    // 如果是免登陆的页面则直接进入，否则跳转到登录页面
    if (whiteList.indexOf(to.path) >= 0) {
      console.log('页面无需登录即可访问')
      NProgress.start()
      next()
    } else {
      console.log('重新登录')
      router.replace('/login')
    }
  }
})

/**
 * onError
 */
router.onError((error) => {
  console.log('页面跳转出错onError:', error)
})

/**
 *  动态路由
 * @param permission 权限树
 * @param router 路由
 * @returns {Promise<any>} 新路由对象
 */
function routerMatch (permission, router) {
  return new Promise((resolve) => {
    let _routes = {
      path: '/main',
      component: r => require.ensure([], () => r(require('../views/main')), '管理系统主页面'),  //  管理系统主页面
      children: [
      ]
    }
    console.log('动态路由-要加工数据:', permission)
    // 创建路由
    function createRouter (permission) {
      permission.forEach((item) => {
        if (item.filePath !== '') {
          let _route = {
            path: item.href,
            name: item.name,
            meta: {
              isClear: item.isClear,
              isShow: item.isShow,
              icon: item.icon
            },
            component (resolve) {
              require(['../views/' + item.filePath + '.vue'], resolve)
            }
          }
          console.log('路由数据:', _route)
          _routes.children.push(_route)
        }
        if (item.children && item.children.length) {
          // 递归
          createRouter(item.children)
        }
      })
    }
    // OBJECT STRING
    if (typeof permission === 'object') {
      createRouter(permission)
    }
    console.log('动态路由-加工后数据:', _routes)
    resolve([_routes])
  })
}

/**
 * 路由跳转后
 */
router.afterEach(() => {
  NProgress.done()
})

export default router
