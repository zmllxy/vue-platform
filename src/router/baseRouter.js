// 默认路由表
const routes = [
  {
    path: '/',
    // 重定向
    redirect: '/main'
  },
  {
    path: '/login',
    component: r => require.ensure([], () => r(require('../views/login')), '登录页面') //  登录页面
  },
  {
    path: '/main',
    component: r => require.ensure([], () => r(require('../views/main')), '管理系统主页面'),  //  管理系统主页面
    children: [
      {
        path: '/error/builds',
        component: r => require.ensure([], () => r(require('../views/error/builds')), '页面建设中') //  页面建设中
      },
      {
        path: '/error/401',
        component: r => require.ensure([], () => r(require('../views/error/401')), '401错误页面') //  401错误页面
      },
      {
        path: '/error/403',
        component: r => require.ensure([], () => r(require('../views/error/403')), '403错误页面') //  403错误页面
      },
      {
        path: '/error/404',
        component: r => require.ensure([], () => r(require('../views/error/404')), '404错误页面') //  404错误页面
      },
      {
        path: '/error/500',
        component: r => require.ensure([], () => r(require('../views/error/500')), '500错误页面') //  500错误页面
      }
    ]
  }
]

export default routes
