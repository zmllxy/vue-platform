/**
 * 只保留菜单
 *
 * @param treeArray
 * @param vaule
 * @param delParam
 * @param childrenPropName
 * @returns {*}
 */
export const filterObjByParam = (treeArray, vaule = 1, delParam = 'menuType', childrenPropName = 'children') => {
  if (!treeArray || treeArray == null || treeArray.length <= 0) {
    return treeArray
  }
  return treeArray.filter(function (item, index) {
    if (item[childrenPropName]) {
      item[childrenPropName] = filterObjByParam(item[childrenPropName], vaule, delParam, childrenPropName)
    }
    return item[delParam] === vaule
  })
}

export default {
  filterObjByParam
}
