import permissionTypes from './modules/sysm/permission/permissionTypes'
const types = {
  FS: '/',
  // /** 权限 Module */
  // PERMISSION: 'permission',
  // /** 侧边开关  Getter */
  // SIDEBAR: 'sidebar',
  // /** 屏幕信息  Mutation */
  // VIEW_INFO: 'viewInfo',
  // /** 菜单 Mutation */
  // MENU_LIST: 'menuList',
  // /** 选择路由 Mutation*/
  // TABS_ROUTER: 'tabsRouter',
  // /** 清空保存选择路由 Action */
  // CLEAR_TABS_ROUTER: 'clearTabsRouter',
  // /** 清空保存菜单 Action */
  // CLEAR_MENU_LIST: 'clearMenuList',
  // /** 移除选择路由 Mutation */
  // DEL_TABS_ROUTER: 'delTabsRouter'
  ...permissionTypes
}

export default types
