import user from './sysm/user'
import permission from './sysm/permission'

export default {
  user: user,
  permission: permission
}
