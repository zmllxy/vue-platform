import Mock from 'mockjs'
import navlist from './sysm/navlist'
import login from './sysm/login'

let data = [].concat(
  navlist,
  login
)

data.forEach(function (res) {
  Mock.mock(res.path, res.data)
})

export default Mock
