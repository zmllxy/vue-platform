import pathToRegexp from 'path-to-regexp'

/**
 * Session 操作
 *
 * @param key 键
 * @param defaultValue 默认值
 * @returns {*} 返回对象时JSON处理下
 */
export const getSessionKey = (key, defaultValue) => {
  const item = window.sessionStorage.getItem(key)
  console.log('getSessionKey', item, defaultValue)
  if ((item === undefined || item === null) && defaultValue !== undefined) {
    console.log('defaultValue', defaultValue)
    return defaultValue
  }
  return JSON.parse(item)
}

/**
 * 根据id查询树对象
 *
 * @param treeArray
 * @param id
 * @param idPropName
 * @param childrenPropName
 * @returns {*}
 */
export const findFromTree = (treeArray, id, idPropName = 'id', childrenPropName = 'children') => {
  if (!treeArray || treeArray == null || treeArray.length <= 0) {
    return null
  }
  for (var i = 0; i < treeArray.length; i++) {
    if (treeArray[i][idPropName] === id) {
      return treeArray[i]
    } else {
      let result = findFromTree(treeArray[i][childrenPropName], id, idPropName, childrenPropName)
      if (result !== null) {
        return result
      }
    }
  }
  return null
}

/**
 * 只保留菜单
 *
 * @param treeArray
 * @param vaule
 * @param delParam
 * @param childrenPropName
 * @returns {*}
 */
export const delMenuTreeButton = (treeArray, vaule = 1, delParam = 'menuType', childrenPropName = 'children') => {
  if (!treeArray || treeArray == null || treeArray.length <= 0) {
    return treeArray
  }
  return treeArray.filter(function (item, index) {
    if (item[childrenPropName]) {
      item[childrenPropName] = delMenuTreeButton(item[childrenPropName], vaule, delParam, childrenPropName)
    }
    return item[delParam] === vaule
  })
}

export const getBaseUrl = (url) => {
  // eslint-disable-next-line
  var reg = /^((\w+):\/\/([^\/:]*)(?::(\d+))?)(.*)/
  reg.exec(url)
  return RegExp.$1
}

/**
 * 数组格式转树状结构
 * @param   {array}     array
 * @param   {String}    id
 * @param   {String}    pid
 * @param   {String}    children
 * @return  {Array}
 */
export const arrayToTree = (array, id = 'id', pid = 'pid', children = 'children') => {
  let data = array.map(item => ({ ...item }))
  let result = []
  let hash = {}
  data.forEach((item, index) => {
    hash[ data[ index ][ id ] ] = data[ index ]
  })

  data.forEach((item) => {
    let hashVP = hash[ item[ pid ] ]
    if (hashVP) {
      !hashVP[ children ] && (hashVP[ children ] = [])
      hashVP[ children ].push(item)
    } else {
      result.push(item)
    }
  })
  return result
}

export function getCurrentMenu (location, arrayMenu) {
  if (!arrayMenu) {
    let current = []
    for (let i = 0; i < arrayMenu.length; i++) {
      const e = arrayMenu[ i ]
      const child = getCurrentMenu(location, e.children)
      if (!child && child.length > 0) {
        child.push({ ...e, children: null })
        return child
      }
      if (e.href && pathToRegexp(e.href).exec(location)) {
        current.push({ ...e, children: null })
        return current
      }
    }
    return current
  }
  return null
}
