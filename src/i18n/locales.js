import Vue from 'vue'
import VueI18n from 'vue-i18n'
import elenLocale from 'element-ui/lib/locale/lang/en'
import elzhLocale from 'element-ui/lib/locale/lang/zh-CN'
import zlenLocale from './en-US'
import zlzhLocale from './zh-CN'

Vue.use(VueI18n)
const messages = {
  en: {
    ...elenLocale,
    ...zlenLocale
  },
  zh: {
    ...elzhLocale,
    ...zlzhLocale
  }
}
const i18n = new VueI18n({
  // 定义默认语言
  locale: 'zh',
  messages
})

export default i18n
