import ZlPanel from './panel'

ZlPanel.install = function (Vue) {
  Vue.component(ZlPanel.name, ZlPanel)
}

export default ZlPanel
