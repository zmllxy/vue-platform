// 组件全局注册
import Vue from 'vue'

import NavItem from './navItem'
import HeadTop from './headTop'
import ZlPanel from './panel'
import ElSelectTree from './selectTree'
import SubMenu from './subMenu'
import SideMenu from './sideMenu'
import ZlRouterTabs from './routerTabs'

// 形成组件库
const components = [
  NavItem,
  HeadTop,
  ZlPanel,
  ElSelectTree,
  SubMenu,
  SideMenu,
  ZlRouterTabs
]

// 注册全局组件
components.map((com) => {
  Vue.use(com)
})

export default components
