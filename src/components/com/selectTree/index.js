import ElSelectTree from './selectTree'

ElSelectTree.install = function (Vue) {
  Vue.component(ElSelectTree.name, ElSelectTree)
}

export default ElSelectTree
