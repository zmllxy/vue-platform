import SubMenu from './subMenu'

SubMenu.install = function (Vue) {
  Vue.component(SubMenu.name, SubMenu)
}

export default SubMenu
