import SideMenu from './sideMenu'

SideMenu.install = function (Vue) {
  Vue.component(SideMenu.name, SideMenu)
}

export default SideMenu
