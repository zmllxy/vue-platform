import ZlRouterTabs from './zlRouterTabs'

ZlRouterTabs.install = function (Vue) {
  Vue.component(ZlRouterTabs.name, ZlRouterTabs)
}

export default ZlRouterTabs
