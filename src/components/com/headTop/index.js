import HeadTop from './headTop'

HeadTop.install = function (Vue) {
  Vue.component(HeadTop.name, HeadTop)
}

export default HeadTop
