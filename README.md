# 系统平台[vue-platform](http://127.0.0.1:8088)

[![npm](https://img.shields.io/npm/v/vue-access-control.svg)](https://www.npmjs.com/package/vue-access-control/)  [![license](https://img.shields.io/github/license/tower1229/Vue-Access-Control.svg)]()
> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

>For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

>You may use special comments to disable some warnings.
>Use // eslint-disable-next-line to ignore the next line.
>Use /* eslint-disable */ to ignore all warnings in a file.

# Vue项目创建过程
## markdown
>[在线编辑器地址](http://mahua.jser.me/)

## npm安装模块
- npm install xxx 利用 npm 安装xxx模块到当前命令行所在目录
- npm install -g xxx 利用npm安装全局模块xxx

## 本地安装时将模块写入package.json中
- npm install xxx 安装但不写入package.json
- npm install xxx –save 安装并写入package.json的‘dependencies’中
- npm install xxx –save-dev 安装并写入package.json的‘devDependencies’中

## npm 删除模块
- npm uninstall xxx 删除xxx模块
- npm uninstall -g xxx 删除全局模块xxx

## 淘宝 NPM 镜像
>使用说明 [网址](http://npm.taobao.org/)
- 安装 npm install -g cnpm --registry=https://registry.npm.taobao.org

## Vue CLI 1/2/3
>使用说明 [CLI2-网址vue-cli](https://cli.vuejs.org/zh/guide/)--[CLI3-网址@vue/cli](https://cli.vuejs.org/zh/guide/)
- 安装 cnpm install vue-cli -g
- 安装 cnpm install -g @vue/cli

## ICON
http://www.iconfont.cn/

## 项目用到插件
>1. 插件网址[vue-router](https://router.vuejs.org/zh/)
   安装 cnpm install vue-router -S
>1. 插件网址[qs](https://www.npmjs.com/package/qs)
   安装 cnpm install qs -S
>1. 插件网址[element-ui](http://element-cn.eleme.io/#/zh-CN)
   安装 cnpm i element-ui -S
>1. 插件网址[nprogress](https://www.npmjs.com/package/nprogress)
    安装 cnpm i nprogress -S
>1. 插件网址[axios](https://www.npmjs.com/package/axios)
    安装 cnpm i axios -S
>1. 插件网址[vuex](https://vuex.vuejs.org/zh/)
    安装 cnpm i vuex -S
>1. 插件网址[mockjs](http://mockjs.com/)
    安装 npm install express http body-parser superagent mockjs --save-dev
>1. 插件网址[font-awesome](http://fontawesome.dashgame.com/)
    安装 cnpm install font-awesome -S
>1. 插件网址[path-to-regexp](https://www.npmjs.com/package/path-to-regexp)
    安装 cnpm i path-to-regexp -S
>1. 插件网址[vue-i18n](http://kazupon.github.io/vue-i18n/)
    安装 cnpm i -S vue-i18n
>1. 插件网址[vuescroll](http://vuescrolljs.yvescoding.org/zh/)
    安装 cnpm install vuescroll -S
>1. 插件网址[lodash](https://www.npmjs.com/package/lodash)
    安装 cnpm i --save lodash
    
    cnpm install perfect-scrollbar --save
    cnpm install classlist-polyfill --save

## Tabs 组件开发

## Panel 组件开发
    - vue-panel-split  cnpm i vue-panel-split -S
    - cnpm i asva-double-panel -S
## 联系方式
* QQ: 137127502
* 邮件(zl_xinyu@163.com)
* 微信公众号二维码
* 博客
